# Scala + SBT + Docker Dockerfile
# Highly based on excellent hseeberger/scala-sbt
# https://github.com/hseeberger/scala-sbt/blob/485c93cf67521685622700e3b1c3a85de90a4d12/debian/Dockerfile

ARG DOCKER_VERSION
FROM docker:${DOCKER_VERSION}

ARG JAVA_MAJOR_VERSION
ENV JAVA_MAJOR_VERSION ${JAVA_MAJOR_VERSION:-11}
ARG JAVA_PACKAGE_TYPE
ENV JAVA_PACKAGE_TYPE ${JAVA_PACKAGE_TYPE:-jdk}
ARG SCALA_VERSION
ENV SCALA_VERSION ${SCALA_VERSION:-2.13.8}
ARG SBT_VERSION
ENV SBT_VERSION ${SBT_VERSION:-1.6.2}
ARG USER_ID
ENV USER_ID ${USER_ID:-1001}
ARG GROUP_ID
ENV GROUP_ID ${GROUP_ID:-1001}

# Bash is later on required by Scala and SBT
# Installing it as one of the first steps in order to favor docker layer reuse
RUN apk add -q bash

## Start of Temurin JDK installation
## As per https://github.com/adoptium/containers/blob/7b5414180352a1eb26be18e16d61acd81ca4ea55/11/jdk/alpine/Dockerfile.releases.full#L22-L61
ENV JAVA_HOME /opt/java/openjdk
ENV PATH $JAVA_HOME/bin:$PATH

# Default to UTF-8 file.encoding
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

# fontconfig and ttf-dejavu added to support serverside image generation by Java programs
RUN apk add --no-cache fontconfig libretls musl-locales musl-locales-lang ttf-dejavu tzdata zlib \
    && rm -rf /var/cache/apk/*

ENV JAVA_VERSION jdk-11.0.17+8

RUN set -eux; \
    ARCH="$(apk --print-arch)"; \
    case "${ARCH}" in \
      amd64|x86_64) \
        case "${JAVA_PACKAGE_TYPE}" in \
          jdk) \
            case "${JAVA_MAJOR_VERSION}" in \
              11) \
                # Actual values retrieved from
                # https://github.com/adoptium/containers/blob/7b5414180352a1eb26be18e16d61acd81ca4ea55/11/jdk/alpine/Dockerfile.releases.full#L38-L39
                ESUM='774d5955c09893dda14e3eb0fd3e239a6b2cec58615fcf4ec68747260b6e1cc1'; \
                BINARY_URL='https://github.com/adoptium/temurin11-binaries/releases/download/jdk-11.0.17%2B8/OpenJDK11U-jdk_x64_alpine-linux_hotspot_11.0.17_8.tar.gz'; \
              ;; \
              17) \
                # Actual values retrieved from
                # https://github.com/adoptium/containers/blob/7b5414180352a1eb26be18e16d61acd81ca4ea55/17/jdk/alpine/Dockerfile.releases.full#L38-L39
                ESUM='cb154396ff3bfb6a9082e3640c564643d31ecae1792fab0956149ed5258ad84b'; \
                BINARY_URL='https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.5%2B8/OpenJDK17U-jdk_x64_alpine-linux_hotspot_17.0.5_8.tar.gz'; \
              ;; \
              21) \
                # Actual values retrieved from
                # https://github.com/adoptium/containers/blob/cea001c89963480665fb78603d07e93f55c6991e/21/jdk/alpine/Dockerfile#L63-L64
                ESUM='8da7da49101d45f646272616f20e8b10d57472bbf5961d64ffb07d7ba93c6909'; \
                BINARY_URL='https://github.com/adoptium/temurin21-binaries/releases/download/jdk-21.0.5%2B11/OpenJDK21U-jdk_x64_alpine-linux_hotspot_21.0.5_11.tar.gz'; \
              ;; \
              *) \
                echo "Unsupported Java Major Version: ${JAVA_MAJOR_VERSION}. Supported version are: 11, 17, 21"; \
                exit 1; \
              ;; \
            esac; \
            ;; \
          jre) \
            case "${JAVA_MAJOR_VERSION}" in \
              11) \
                # Actual values retrieved from
                # https://github.com/adoptium/containers/blob/7b5414180352a1eb26be18e16d61acd81ca4ea55/11/jre/alpine/Dockerfile.releases.full#L38-L39
                ESUM='96d26887d042f3c5630cca208b6cd365679a59bf9efb601b28363e827439796c'; \
                BINARY_URL='https://github.com/adoptium/temurin11-binaries/releases/download/jdk-11.0.17%2B8/OpenJDK11U-jre_x64_alpine-linux_hotspot_11.0.17_8.tar.gz'; \
              ;; \
              17) \
                # Actual values retrieved from
                # https://github.com/adoptium/containers/blob/7b5414180352a1eb26be18e16d61acd81ca4ea55/17/jre/alpine/Dockerfile.releases.full#L38-L39
                ESUM='56daddc4c38cda4fa8716d0a6c5b3197305b94ed7011f06adfcd55357952ae17'; \
                BINARY_URL='https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.5%2B8/OpenJDK17U-jre_x64_alpine-linux_hotspot_17.0.5_8.tar.gz'; \
              ;; \
              21) \
                # Actual values retrieved from
                # https://github.com/adoptium/containers/blob/cea001c89963480665fb78603d07e93f55c6991e/21/jre/alpine/Dockerfile#L60-L61
                ESUM='2dfa33fb8e9474e6967c6cf17964abb5ddce9c17fa6a9f8d7aa221a0ae295df9'; \
                BINARY_URL='https://github.com/adoptium/temurin21-binaries/releases/download/jdk-21.0.5%2B11/OpenJDK21U-jre_x64_alpine-linux_hotspot_21.0.5_11.tar.gz'; \
              ;; \
              *) \
                echo "Unsupported Java Major Version: ${JAVA_MAJOR_VERSION}. Supported version are: 11, 17, 21"; \
                exit 1; \
              ;; \
            esac; \
            ;; \
          *) \
            echo "Unsupported Java package type: ${JAVA_PACKAGE_TYPE}. Supported types are: jre, jdk"; \
            exit 1; \
            ;; \
        esac; \
        ;; \
      *) \
        echo "Unsupported arch: ${ARCH}"; \
        exit 1; \
        ;; \
    esac; \
    wget -O /tmp/openjdk.tar.gz ${BINARY_URL}; \
    echo "${ESUM} */tmp/openjdk.tar.gz" | sha256sum -c -; \
    mkdir -p "$JAVA_HOME"; \
    tar --extract \
        --file /tmp/openjdk.tar.gz \
        --directory "$JAVA_HOME" \
        --strip-components 1 \
        --no-same-owner \
    ; \
    rm -f /tmp/openjdk.tar.gz ${JAVA_HOME}/src.zip;

RUN echo Verifying install ...; \
    case "${JAVA_PACKAGE_TYPE}" in \
      jdk) \
        fileEncoding="$(echo 'System.out.println(System.getProperty("file.encoding"))' | jshell -s -)"; [ "$fileEncoding" = 'UTF-8' ]; rm -rf ~/.java; \
        echo javac --version; javac --version; \
        ;; \
      jre) \
        ;; \
      *) \
        echo "Unsupported Java package type: ${JAVA_PACKAGE_TYPE}. Supported types are: jre, jdk"; \
        exit 1; \
        ;; \
    esac; \
    echo java --version; java --version; \
    echo Complete.;
## End of Temurin JDK installation

# Scala and SBT requires bash
SHELL ["/bin/bash", "-c"]

# Install sbt
RUN \
  wget -qO - "https://github.com/sbt/sbt/releases/download/v$SBT_VERSION/sbt-$SBT_VERSION.tgz" | tar xfz - -C /usr/share && \
  chown -R root:root /usr/share/sbt && \
  chmod -R 755 /usr/share/sbt && \
  ln -s /usr/share/sbt/bin/sbt /usr/local/bin/sbt

# Install Scala
RUN \
  case $SCALA_VERSION in \
    "3"*) URL=https://github.com/lampepfl/dotty/releases/download/$SCALA_VERSION/scala3-$SCALA_VERSION.tar.gz SCALA_DIR=/usr/share/scala3-$SCALA_VERSION ;; \
    *) URL=https://downloads.typesafe.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz SCALA_DIR=/usr/share/scala-$SCALA_VERSION ;; \
  esac && \
  wget -qO - $URL | tar xfz - -C /usr/share && \
  mv $SCALA_DIR /usr/share/scala && \
  chown -R root:root /usr/share/scala && \
  chmod -R 755 /usr/share/scala && \
  ln -s /usr/share/scala/bin/* /usr/local/bin && \
  mkdir -p /test && \
  case $SCALA_VERSION in \
    "3"*) echo 'import java.io.FileInputStream;import java.util.jar.JarInputStream;val scala3LibJar = classOf[CanEqual[_, _]].getProtectionDomain.getCodeSource.getLocation.toURI.getPath;val manifest = new JarInputStream(new FileInputStream(scala3LibJar)).getManifest;val ver = manifest.getMainAttributes.getValue("Implementation-Version");@main def main = println(s"Scala version ${ver}")' > /test/test.scala ;; \
    *) echo "println(util.Properties.versionMsg)" > /test/test.scala ;; \
  esac && \
  scala -nocompdaemon test/test.scala && \
  rm -fr test

# Add and use user sbtuser
RUN addgroup -g $GROUP_ID sbtuser && adduser -G sbtuser -u $USER_ID -S sbtuser --shell /bin/bash
USER sbtuser

# Switch working directory
WORKDIR /home/sbtuser

# Prepare sbt (warm cache)
RUN \
  sbt --script-version && \
  mkdir -p project && \
  echo "scalaVersion := \"${SCALA_VERSION}\"" > build.sbt && \
  echo "sbt.version=${SBT_VERSION}" > project/build.properties && \
  echo "case object Temp" > Temp.scala && \
  echo "// force sbt compiler-bridge download" > project/Dependencies.scala && \
  sbt compile && \
  rm -r project && rm build.sbt && rm Temp.scala && rm -r target

# Link everything into root as well
# This allows users of this container to choose, whether they want to run the container as sbtuser (non-root) or as root
USER root
RUN \
  ln -s /home/sbtuser/.cache /root/.cache && \
  ln -s /home/sbtuser/.sbt /root/.sbt && \
  if [ -d "/home/sbtuser/.ivy2" ]; then ln -s /home/sbtuser/.ivy2 /root/.ivy2; fi

# Switch working directory back to root
## Users wanting to use this container as non-root should combine the two following arguments
## -u sbtuser
## -w /home/sbtuser
WORKDIR /root

CMD sbt
