# Docker + Scala + sbt Dockerfile

This repository contains **Dockerfile** of [Scala](http://www.scala-lang.org), [sbt](http://www.scala-sbt.org) and
[Docker](https://hub.docker.com/_/docker) itself.

# Goal

This image has been originally created to support
[SBT Native Packager](https://www.scala-sbt.org/sbt-native-packager/index.html)'s
[Docker Plugin](https://www.scala-sbt.org/sbt-native-packager/formats/docker.html) which requires docker's binaries.

# Requirements

- [Docker](https://www.docker.com/)

# Build

```
$ docker build . \
--build-arg DOCKER_VERSION=20.10.8 \
--build-arg JAVA_MAJOR_VERSION=11 \
--build-arg JAVA_PACKAGE_TYPE=jdk \
--build-arg SCALA_VERSION=2.13.6 \
--build-arg SBT_VERSION=1.5.5 \
--tag docker-scala-sbt:20.10.8_temurin-11-jdk_2.13.6_1.5.5
```

`JAVA_PACKAGE_TYPE` supported values are "jdk" and "jre".

## Usage

```
$ docker run -it docker-scala-sbt:20.10.8_temurin-11-jdk_2.13.6_1.5.5
[info] Updated file /root/project/build.properties: set sbt.version to 1.5.5
[info] welcome to sbt 1.5.5 (Eclipse Adoptium Java 11.0.17)
[info] loading project definition from /root/project
[info] set current project to root (in build file:/root/)
[info] sbt server started at local:///root/.sbt/1.0/server/27dc1aa3fdf4049b492d/sock
[info] started sbt server
sbt:root> 
```

```
$ docker run -it docker-scala-sbt:20.10.8_temurin-11-jdk_2.13.6_1.5.5 scala
Welcome to Scala 2.13.6 (OpenJDK 64-Bit Server VM, Java 11.0.17).
Type in expressions for evaluation. Or try :help.

scala> 

```

```
$ docker run -it docker-scala-sbt:20.10.8_temurin-11-jdk_2.13.6_1.5.5 bash
bash-5.1# docker --version
Docker version 20.10.8, build 3967b7d
bash-5.1# 
bash-5.1# java --version
openjdk 11.0.17 2022-10-18
OpenJDK Runtime Environment Temurin-11.0.17+8 (build 11.0.17+8)
OpenJDK 64-Bit Server VM Temurin-11.0.17+8 (build 11.0.17+8, mixed mode)
bash-5.1# 
bash-5.1# javac --version
javac 11.0.17
bash-5.1# 
bash-5.1# scala --version
Scala code runner version 2.13.6 -- Copyright 2002-2021, LAMP/EPFL and Lightbend, Inc.
bash-5.1# 
bash-5.1# sbt --version
sbt version in this project: 1.5.5
sbt script version: 1.5.5
bash-5.1# 

```
